#!/usr/bin/env less
#
# Tutorial about getting started with android App development from the command
# line shell.
#
# Similar to
# http://vishalraj.in/blogs/hello-world-writing-my-first-android-app-on-linux

ANDROID_BASE_DIR=$HOME/Android

mkdir $ANDROID_BASE_DIR
cd $ANDROID_BASE_DIR

# Download and unpack the SDK from
# http://developer.android.com/sdk/index.html
wget http://dl.google.com/android/android-sdk_r24.3.4-linux.tgz
tar xzvf android-sdk_r24.3.4-linux.tgz

# Add "platform-tools" and "tools" to the PATH
export ANDROID_HOME=$ANDROID_BASE_DIR/android-sdk-linux
export PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools

# List packages
android list sdk --all --extended

# Install packages. Use this same command line to update the packages
android update sdk --no-ui --all --filter tools,platform-tools,build-tools-23.0.1,android-17,sys-img-x86-android-17,extra-android-support

# Check what targets are available
android list targets

# Create an Android Virtual Device (AVD)
android create avd --target android-17 --name android-17-x86 --abi x86

# Create a Hello World application
# http://developer.android.com/tools/projects/projects-cmdline.html
#
# Info on the latest version of the Android Gradle Plugin here:
# https://developer.android.com/tools/revisions/gradle-plugin.html
mkdir $ANDROID_BASE_DIR/Apps
android create project \
	--gradle --gradle-version 1.3.1 \
	--target android-17 \
	--name MyFirstApp \
	--path $ANDROID_BASE_DIR/Apps/MyFirstApp \
	--activity MainActivity \
	--package com.example.myfirstapp

# Set a gradle version compatible with the Android Gradle Plugin used above,
# see: http://tools.android.com/tech-docs/new-build-system/version-compatibility
sed -e '/distributionUrl/s/gradle-[^\-]*-all/gradle-2\.5-all/g' -i $ANDROID_BASE_DIR/Apps/MyFirstApp/gradle/wrapper/gradle-wrapper.properties
sed -e 's/runProguard false/minifyEnabled false/g' -i $ANDROID_BASE_DIR/Apps/MyFirstApp/build.gradle

# And maybe you want to use git for your App?
cd $ANDROID_BASE_DIR/Apps/MyFirstApp
rm local.properties
git init
git add .
git commit -m $'Initial import\n\nhttp://developer.android.com/training/basics/firstapp/creating-project.html'
echo ".gradle/" > .gitignore
echo "build/" >> .gitignore
echo "local.properties" >> .gitignore
git add .gitignore
git commit -m "Add a .gitignore file"

# Learn how to write Android Apps:
xdg-open http://developer.android.com/training/basics/firstapp/building-ui.html

# Build the App
# http://developer.android.com/tools/building/building-cmdline.html
cd $ANDROID_BASE_DIR/Apps/MyFirstApp
./gradlew assembleDebug

# Start the emulator, hardware accelerated:
# http://developer.android.com/tools/devices/emulator.html#vm-linux
emulator -verbose -avd android-17-x86 -scale 0.9 -gpu on -qemu -m 512 -enable-kvm

# Install the App into an Android [Virtual] Device
adb devices -l
adb -s emulator-5554 install build/outputs/apk/MyFirstApp-debug.apk

# Launch your application from the HOST
adb -s emulator-5554 -e shell am start -a android.intent.action.MAIN -n com.example.myfirstapp/com.example.myfirstapp.MainActivity

# See logs, e.g. only errors
adb -s emulator-5554 logcat *:E

# Connect to the emulator via telnet if needed
telnet localhost 5554

