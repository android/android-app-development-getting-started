#!/bin/sh

set -e

if command -v xmllint >/dev/null 2>&1;
then
  # Try xmllint from the "xmllint" package
  XPATH="xmllint --xpath "
elif command -v xpath >/dev/null 2>&1;
then
  # xpath is from the perl XML::XPath module: libxml-xpath-perl package
  XPATH="xpath -e"
else
  { echo "Install either xmllint or the XML::XPath perl module" 1>&2; exit 1; }
fi

APP_NAME=$($XPATH "string(//string[@name = 'app_name'])" src/main/res/values/strings.xml)
PACKAGE=$($XPATH "string(//manifest/@package)" src/main/AndroidManifest.xml)
MAIN_ACTIVITY=$($XPATH "string(//activity[1]/@*[local-name() = 'name'])" src/main/AndroidManifest.xml)

./gradlew assembleDebug
adb -s emulator-5554 install -r build/outputs/apk/${APP_NAME}-debug.apk
adb -s emulator-5554 -e shell am start -a android.intent.action.MAIN -n ${PACKAGE}/${PACKAGE}.${MAIN_ACTIVITY}
