The template provided here is 48x48 px with a border of 1/12 of the size.
http://tekeye.biz/2012/android-launcher-icons-using-inkscape

The exporter script will generate icons at 36x36, 48x48, 72x72, 96x96 pixels

The androink.py export script is an evolution of:
https://gist.github.com/fedepaol/4127778

And alternative is to use the android4inkscape extension:
https://github.com/kengoodridge/android4inkscape
