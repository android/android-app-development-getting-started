#!/usr/bin/env python
#
# androink - Generate android resources for multiple densities
#
# Copyright (C) 2012  Federico Paolinelli <fedepaol@gmail.com>
# Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Based on:
# https://gist.github.com/fedepaol/4127778
#
# Following the directions of:
# https://developer.android.com/guide/practices/screens_support.html

import argparse
import sys
import os

__description = """This script expects the svg to use Inkscape DPIs.

Either MDPI or HDPI can be used as the baseline density and the drawables will
scaled to generate the other densities.

"""
__version = "0.1"
__author_info = "Federico Paolinelli, Antonio Ospite"


BASE_OPTIONS = ' --export-area-page --export-png '

# Inkscape default DPI is 90
BASELINE_DPI = 90

# densities are calculated following the scaling ratio
# 3:4:6:8 mapped to ldpi:mdpi:hdpi:xhdpi

# for baseline mdpi consider 4 as the base ratio
densities_baseline_mdpi = [
    ('drawable-ldpi', BASELINE_DPI * 0.75),    # 3/4
    ('drawable-mdpi', BASELINE_DPI * 1.0),     # 4/4
    ('drawable-hdpi', BASELINE_DPI * 1.5),     # 6/4
    ('drawable-xhdpi', BASELINE_DPI * 2.0),    # 8/4
    ('drawable-xxhdpi', BASELINE_DPI * 3.0),   # 12/4
    ('drawable-xxxhdpi', BASELINE_DPI * 4.0),  # 16/4
]

# for baseline hdpi consider 6 as the base ratio
densities_baseline_hdpi = [
    ('drawable-ldpi', BASELINE_DPI * 0.5),             # 3/6
    ('drawable-mdpi', BASELINE_DPI * 0.666666667),     # 4/6
    ('drawable-hdpi', BASELINE_DPI * 1.0),             # 6/6
    ('drawable-xhdpi', BASELINE_DPI * 1.333333333),    # 8/6
    ('drawable-xxhdpi', BASELINE_DPI * 2.0),           # 12/6
    ('drawable-xxxhdpi', BASELINE_DPI * 2.666666667),  # 16/6
]


def export_file(file_name):
    print 'exporting file', file_name
    name_without_ext = os.path.splitext(file_name)[0]

    if args.baseline_density == 'mdpi':
        densities = densities_baseline_mdpi
    elif args.baseline_density == 'hdpi':
        densities = densities_baseline_hdpi

    for density in densities:
        dpispec, dpi = density
        res_path = os.path.join(args.res_folder, dpispec)

        if not os.path.exists(res_path):
            os.makedirs(res_path)

        source_file = os.path.join(args.svg_folder, file_name)
        target = os.path.join(res_path, name_without_ext + '.png')

        command_list = [args.ink_path, '--export-area-page',
                        '-f', source_file,
                        '--export-png', target,
                        '--export-dpi', str(dpi)]

        command = " ".join(command_list)

        print 'executing', command
        if not args.dry:
            os.popen(command)


def option_parser():
    usage = "usage: %(prog)s [options]"

    parser = argparse.ArgumentParser(usage=usage,
                                     description=__description,
                                     epilog=__author_info,
                                     version='%(prog)s ' + __version,)

    parser.add_argument('-R', '--res_folder',  metavar="<dir>",
                        dest='res_folder', required=True,
                        help='path to the project res folder')

    parser.add_argument('-S', '--svg_folder', metavar="<dir>",
                        dest='svg_folder', default='.',
                        help='folder that contains all the svg files to be converted')

    parser.add_argument('-I', '--inkscape_path', metavar="<inkscape_path>",
                        dest='ink_path', default='inkscape',
                        help='path of Inkscape executable')

    parser.add_argument('-d', '--baseline-density', metavar="<mdpi|hdpi>",
                        dest='baseline_density', choices=['mdpi', 'hdpi'], default='mdpi',
                        help='the baseline density to generate the drawables from')

    parser.add_argument('-D', '--dry_run',
                        dest='dry', action='store_const', const=True,
                        help='performs a dry run')

    parser.add_argument('-F', '--single_file', metavar="<file>",
                        dest='file_name',
                        help='name of the file, if only one file is to be converted')

    return parser


if __name__ == "__main__":
    parser = option_parser()
    args = parser.parse_args()

    if args.file_name:
        export_file(args.file_name)
    else:
        files = os.listdir(args.svg_folder)
        svg_files = filter(lambda x: x.lower().endswith('.svg'), files)

        map(export_file, svg_files)
